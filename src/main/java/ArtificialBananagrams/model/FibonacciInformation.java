package ArtificialBananagrams.model;

public class FibonacciInformation {
  private int progress;
  private Integer result;
  public FibonacciInformation() {
    progress = 0;
    result = null;
  }

  public synchronized int getProgress() { return progress;}
  public synchronized void setProgress(int num) { progress = num; }

  public synchronized int getResult() {
    if(result == null) return -1;
    return result;
  }
  public synchronized void setResult(int res) {
    result = res;
  }

}
