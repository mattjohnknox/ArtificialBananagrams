package ArtificialBananagrams.model;

public class Model {


  private FibonacciCalculator calculator;

  public Model() {
    calculator = new FibonacciCalculator();
  }

  public void startCalculation(int num) {
    calculator.startCalculation(num);
  }

  public String displayCalculationStatus() {
    if(calculator.getStatus() == FibonacciCalculator.RUNNING)
      return String.format("Calculation has made %d calls", calculator.getNumCalls());
    return String.format("Calculation is complete, result is %d (with %d calls)", calculator.getResult(), calculator.getNumCalls());
  }




}
