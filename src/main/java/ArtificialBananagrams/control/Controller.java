package ArtificialBananagrams.control;

import ArtificialBananagrams.model.Model;
import ArtificialBananagrams.view.View;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Controller implements PropertyChangeListener {

  private Model model;
  private View view;
  private Stage currentStage;
  public static final int FRAMES_PER_SECOND = 60;
  public static final double SECOND_DELAY = 1.0 / FRAMES_PER_SECOND;

  public Controller(Stage stage) {
    createModel();
    createView();
    currentStage = stage;
  }

  private void createModel() {
    model = new Model();
  }

  private void update() {
    view.changeText(model.displayCalculationStatus());
  }

  private void createView() {
    view = new View();
    view.addObserver(this);


    // attach "game loop" to timeline to play it (basically just calling step() method repeatedly forever)
    Timeline animation = new Timeline();
    animation.setCycleCount(Timeline.INDEFINITE);
    animation.getKeyFrames().add(new KeyFrame(Duration.seconds(SECOND_DELAY), e -> {
      update();
    }));
    animation.play();


  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    try {
      MethodInformation info = (MethodInformation) evt.getNewValue();
      Method m = this.getClass().getDeclaredMethod(info.methodName(), info.argTypes());
      m.invoke(this, info.args());
    } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
      e.printStackTrace();
    }
  }

  //called by reflection
  private void updateButton(String first, String second) {
    System.out.printf("Reached updateButton with parameters %s and %s\n", first, second);
  }

  //called by reflection
  private void display(String message) {
    view.changeText(message);
  }

  //called by reflection
  private void mouseClicked(double x, double y) {
    System.out.printf("Reached mouseClicked with parameters %f and %f\n", x, y);
  }

  //called by reflection
  private void keyPressed(String key) {
    System.out.printf("Reached keyPressed with parameter %s\n", key);
  }

  //called by reflection
  private void calculate(int num) {
    model.startCalculation(num);
  }

  public void begin() {
    currentStage.setScene(view.getScene());
    currentStage.show();
  }
}
