package ArtificialBananagrams.model;

public class FibonacciCalculator {

  private FibonacciInformation myInfo;
  public static final String RUNNING = "unfinished";
  public static final String COMPLETE = "finished";

  public FibonacciCalculator() {
    myInfo = new FibonacciInformation();
  }

  public void startCalculation(int num) {
    FibonacciHelper helper = new FibonacciHelper(num);
    Thread thread = new Thread(helper);
    thread.start();
  }

  public String getStatus() {
    if(myInfo.getResult() == -1) return RUNNING;
    return COMPLETE;
  }

  public int getNumCalls() {
    return myInfo.getProgress();
  }

  public int getResult() {
    return myInfo.getResult();
  }

  private class FibonacciHelper implements Runnable {
    private final int NUM_TO_CALCULATE;
    private FibonacciHelper(int calc) {
      NUM_TO_CALCULATE = calc;
    }

    @Override
    public void run() {
      myInfo.setResult(fib(NUM_TO_CALCULATE));
    }

    private int fib(int num) {
      myInfo.setProgress(myInfo.getProgress() + 1);
      if(num == 0 || num == 1) return num;
      return fib(num-1) + fib(num-2);
    }

  }

}
