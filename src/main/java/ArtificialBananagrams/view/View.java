package ArtificialBananagrams.view;

import ArtificialBananagrams.control.MethodInformation;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javafx.scene.control.Button;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class View {

  private Scene myScene;
  private PropertyChangeSupport pcs;
  private Button onlyButton;
  private Text myText;

  public View() {
    myScene = setupGame(400, 400, Color.LIGHTBLUE);
    pcs = new PropertyChangeSupport(this);
  }

  public void addObserver(PropertyChangeListener listener) {
    pcs.addPropertyChangeListener(listener);
  }

  public Scene getScene() { return myScene; }

  private Scene setupGame(int width, int height, Color background) {
    Group root = new Group();


    onlyButton = new Button();
    onlyButton.setText("Calculate!");
    onlyButton.setOnAction(e -> buttonPressed());
    root.getChildren().add(onlyButton);

    myText = new Text(0, 50, "");
    root.getChildren().add(myText);


    Scene scene = new Scene(root, width, height, background);
    // respond to input
    scene.setOnKeyPressed(e -> handleKeyInput(e.getCode()));
    scene.setOnMouseClicked(e -> handleMouseInput(e.getX(), e.getY()));

    return scene;
  }

  public void changeText(String s) {
    myText.setText(s);
  }

  private void buttonPressed() {
    changeText("Calculating fib(40) at " + System.currentTimeMillis());
    pcs.firePropertyChange(null, null, new MethodInformation(
        "calculate",
        new Object[]{40}
    ));
  }

  private void handleMouseInput(Double x, Double y) {
    pcs.firePropertyChange(null, null, new MethodInformation(
        "mouseClicked",
        new Object[]{x, y}
    ));
  }

  private void handleKeyInput(KeyCode code) {
    pcs.firePropertyChange(null, null, new MethodInformation(
        "keyPressed",
        new Object[]{code.getName()}
    ));
  }

}
